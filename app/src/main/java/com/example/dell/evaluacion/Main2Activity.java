package com.example.dell.evaluacion;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.dell.evaluacion.utilidades.Utilidades;

public class Main2Activity extends AppCompatActivity {
    EditText Cedula,Nombre,Apellido,Area,Noficina,Fnacimiento;
    ConexionSQLite conn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        conn =new ConexionSQLite(getApplicationContext(),"bd_usuario",null,1);

        Cedula = (EditText) findViewById(R.id.txtCedulas);
        Nombre = (EditText) findViewById(R.id.txtNombre);
        Apellido = (EditText) findViewById(R.id.txtApellido);
        Area = (EditText) findViewById(R.id.txtDepartamento);
        Noficina = (EditText) findViewById(R.id.txtOficina);
        Fnacimiento = (EditText) findViewById(R.id.txtFeNa);

    }
    public void onClick (View view){
        switch (view.getId()){
            case R.id.btnBuscar:
                consultar();
                break;
        }

    }

    private void consultar() {
        SQLiteDatabase db=conn.getReadableDatabase();
        String[] parametros ={Cedula.getText().toString()};
        String[] campos={Utilidades.CAMPO_NOMBRE,Utilidades.CAMPO_APELLIDO,Utilidades.CAMPO_AREA,Utilidades.CAMPO_OFICINA,Utilidades.CAMPO_FECHA};

        try {
            Cursor cursor = db.query(Utilidades.TABLA_USUARIO,campos,Utilidades.CAMPO_CEDULA+"?",parametros,null,null,null);
            cursor.moveToFirst();
            Nombre.setText(cursor.getString(0));
            Apellido.setText(cursor.getString(1));
            Area.setText(cursor.getString(2));
            Noficina.setText(cursor.getString(3));
            Fnacimiento.setText(cursor.getString(4));
            cursor.close();
        }catch (Exception e){
            Toast.makeText(getApplicationContext(),"El documento no existe",Toast.LENGTH_SHORT).show();
            limpiar();
        }


    }

    private void limpiar() {
        Nombre.setText("");
        Apellido.setText("");
        Area.setText("");
        Noficina.setText("");
        Fnacimiento.setText("");
    }
}
