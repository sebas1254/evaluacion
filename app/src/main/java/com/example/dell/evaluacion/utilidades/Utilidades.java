package com.example.dell.evaluacion.utilidades;

public class Utilidades {

     public static final String TABLA_USUARIO="usuario";
    public static final String CAMPO_ID="id";
    public static final String CAMPO_CEDULA="Cedula";
    public static final String CAMPO_NOMBRE="Nombres";
    public static final String CAMPO_APELLIDO="Apellidos";
    public static final String CAMPO_AREA="Area";
    public static final String CAMPO_OFICINA=" NumeroDeOficina";
    public static final String CAMPO_FECHA="FechaNacimiento";


    public static final  String CREAR_TABLA_USUARIO="CREATE TABLE " +
            ""+TABLA_USUARIO+"("+CAMPO_ID+" INTEGER, "+CAMPO_CEDULA+" TEXT, "+CAMPO_NOMBRE+" TEXT,"+CAMPO_APELLIDO+" TEXT,"+CAMPO_AREA+" TEXT, "+CAMPO_OFICINA+" TEXT, "+CAMPO_FECHA+" TEXT)";

}
