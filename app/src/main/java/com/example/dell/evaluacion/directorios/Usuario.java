package com.example.dell.evaluacion.directorios;

public class Usuario {
    private Integer id;
    private String Cedula;
    private String Nombres;
    private String Apellidos;
    private String Area;
    private String NumeroDeOficina;
    private String FechaNacimiento;

    public Usuario(Integer id, String cedula, String nombres, String apellidos, String area, String numeroDeOficina, String fechaNacimiento) {
        this.id = id;
        Cedula = cedula;
        Nombres = nombres;
        Apellidos = apellidos;
        Area = area;
        NumeroDeOficina = numeroDeOficina;
        FechaNacimiento = fechaNacimiento;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCedula() {
        return Cedula;
    }

    public void setCedula(String cedula) {
        Cedula = cedula;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String nombres) {
        Nombres = nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String apellidos) {
        Apellidos = apellidos;
    }

    public String getArea() {
        return Area;
    }

    public void setArea(String area) {
        Area = area;
    }

    public String getNumeroDeOficina() {
        return NumeroDeOficina;
    }

    public void setNumeroDeOficina(String numeroDeOficina) {
        NumeroDeOficina = numeroDeOficina;
    }

    public String getFechaNacimiento() {
        return FechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        FechaNacimiento = fechaNacimiento;
    }
}
