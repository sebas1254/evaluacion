package com.example.dell.evaluacion;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.dell.evaluacion.utilidades.Utilidades;

public class MainActivity extends AppCompatActivity {
EditText Cedula,Nombre,Apellido,Area,Noficina,Fnacimiento;
Button Pasar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Cedula = (EditText) findViewById(R.id.txtCedula);
        Nombre = (EditText) findViewById(R.id.txtNombres);
        Apellido = (EditText) findViewById(R.id.txtApellidos);
        Area = (EditText) findViewById(R.id.txtArea);
        Noficina = (EditText) findViewById(R.id.txtNumOficina);
        Fnacimiento = (EditText) findViewById(R.id.txtFecha);
        Pasar = (Button) findViewById(R.id.btnConsultar);
        ConexionSQLite conn = new ConexionSQLite(this,"bd_usuario",null,1);

        Pasar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                startActivity(intent);
            }
        });

    }

public void onClick (View view){
        registrarUsuario();
}

    private void registrarUsuario() {
        ConexionSQLite conn = new ConexionSQLite(this,"bd_usuario",null,1);
        SQLiteDatabase db =conn.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Utilidades.CAMPO_CEDULA,Cedula.getText().toString());
        values.put(Utilidades.CAMPO_NOMBRE,Nombre.getText().toString());
        values.put(Utilidades.CAMPO_APELLIDO,Apellido.getText().toString());
        values.put(Utilidades.CAMPO_AREA,Area.getText().toString());
        values.put(Utilidades.CAMPO_OFICINA,Noficina.getText().toString());
        values.put(Utilidades.CAMPO_FECHA,Fnacimiento.getText().toString());

        Long CedulaR = db.insert(Utilidades.TABLA_USUARIO,Utilidades.CAMPO_CEDULA,values);
        Toast.makeText(getApplicationContext(),"Cedula :"+ CedulaR,Toast.LENGTH_SHORT).show();
    }
}
